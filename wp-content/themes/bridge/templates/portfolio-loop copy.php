<?php
global $qode_options_proya;
$id = get_the_ID();


$portfolio_qode_like = "on";
if (isset($qode_options_proya['portfolio_qode_like'])) {
	$portfolio_qode_like = $qode_options_proya['portfolio_qode_like'];
}

//is lightbox turned on for image single project?
$lightbox_single_project = "no";
if (isset($qode_options_proya['lightbox_single_project'])){
	$lightbox_single_project = $qode_options_proya['lightbox_single_project'];
}

//is lightbox turned on for video single project?
$lightbox_video_single_project  = 'no';
if (isset($qode_options_proya['lightbox_video_single_project'])) {
	$lightbox_video_single_project = $qode_options_proya['lightbox_video_single_project'];
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$portfolio_text_follow = "portfolio_single_follow";
if (isset($qode_options_proya['portfolio_text_follow'])) {
    $portfolio_text_follow = $qode_options_proya['portfolio_text_follow'];
}

$porftolio_template = 1;
$portfolio_class = 'portfolio_template_1';
if(get_post_meta(get_the_ID(), "qode_choose-portfolio-single-view", true) != ""){
    $porftolio_template = get_post_meta(get_the_ID(), "qode_choose-portfolio-single-view", true);
    $portfolio_class = 'portfolio_template_'.get_post_meta(get_the_ID(), "qode_choose-portfolio-single-view", true);
}elseif(isset($qode_options_proya['portfolio_style'])){
    $porftolio_template = $qode_options_proya['portfolio_style'];
    $porftolio_class = 'portfolio_template_'.$qode_options_proya['portfolio_style'];
}

$porftolio_single_template = get_post_meta(get_the_ID(), "qode_choose-portfolio-single-view", true);

$columns_number = "v4";
if(get_post_meta(get_the_ID(), "qode_choose-number-of-portfolio-columns", true) != ""){
	if(get_post_meta(get_the_ID(), "qode_choose-number-of-portfolio-columns", true) == 2){
		$columns_number = "v2";
	} else if(get_post_meta(get_the_ID(), "qode_choose-number-of-portfolio-columns", true) == 3){
		$columns_number = "v3";
	} else if(get_post_meta(get_the_ID(), "qode_choose-number-of-portfolio-columns", true) == 4){
		$columns_number = "v4";
	}
}else{
	if(isset($qode_options_proya['portfolio_columns_number'])){
		if($qode_options_proya['portfolio_columns_number'] == 2){
			$columns_number = "v2";
		} else if($qode_options_proya['portfolio_columns_number'] == 3) {
			$columns_number = "v3";
		} else if($qode_options_proya['portfolio_columns_number'] == 4) {
			$columns_number = "v4";
		}
	}
}

$disable_portfolio_single_title_label = true;
if(isset($qode_options_proya['disable_portfolio_single_title_label']) && $qode_options_proya['disable_portfolio_single_title_label'] === 'yes'){
    $disable_portfolio_single_title_label = false;
}
?>
<?php if(post_password_required()) {
	echo get_the_password_form();
} else { ?>
<?php if($porftolio_template != "7"){
       $protocol = is_ssl() ? "https:" : "http:";
	?>

<style>
li.slide.clone.flex-active-slide div#portfolio-content {
}
#slider-two ul.flex-direction-nav {
	display: none;
}
.flexslider .flex-caption {
	transform: translateX(-50%) translateY(-50);
}
.flex-direction-nav {
	visibility: hidden;
}
#flex-nav {
	visibility: visible;
}
.exibition_inner h3.title {
	text-align: left;
	font-size: 14px;
	padding: 5px 0px;
	height: auto;
	text-transform: uppercase;
	color: #000 !important;
}
.exibition_inner h3.title span {
	color: #000;
}
.flex-direction-nav a, .flexslider .flex-prev, .portfolio_slider .flex-prev, .flexslider .flex-next, .portfolio_slider .flex-next, body div.pp_default a.pp_next:after, body a.pp_previous:after, body a.pp_next:after, body a.pp_previous:after, .wpb_gallery .wpb_wrapper .wpb_flexslider .flex-direction-nav a {
	background: transparent !important;
}
.flex-direction-nav a:hover, .flexslider .flex-prev:hover, .portfolio_slider .flex-prev:hover, .flexslider .flex-next:hover, .portfolio_slider .flex-next:hover, body a.pp_next:hover:after, body div.pp_default a.pp_previous:hover:after, body a.pp_next:hover:after, body a.pp_previous:hover:after, .flexslider:hover .flex-direction-nav a.flex-prev:hover, .flexslider:hover .flex-direction-nav a.flex-next:hover, .portfolio_slider:hover .flex-direction-nav a.flex-prev:hover, .portfolio_slider:hover .flex-direction-nav a.flex-next:hover, .wpb_gallery .wpb_flexslider .flex-direction-nav a:hover {
	background: transparent !important;
	border: transparent !important;
}
.portfolio {
	padding: 0 45px;
}
.portfolio .column2 {
	width: 32.33%;
}
#nav-menu-item-15530 a, #nav-menu-item-16153 a
{
    color: #95d0ef !important;	
}
</style>
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/lightbox.min.css">
<script src="<?php bloginfo('template_url')?>/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function ($) {
$('#slider-one').flexslider({
    animation: "fade",
	controlsContainer: $("#flex-nav"),
    customDirectionNav: $("#flex-nav a")
	/*,
	sync: "#slider-two"*/
  });
  
});
</script>
<div class="full_width">
  <div class="full_width_inner">
    <div class="portfolio_container default_template_holder clearfix">
      <h3 class="title"><span>
        <?php the_title();?>
        </span></h3>
    </div>
  </div>
</div>
<div class="two_columns_66_33 clearfix portfolio_container">
  <div class="column1">
    <div class="column_inner">
      <div class="flexslider" id="slider-one">
        <?php if( have_rows('artist_works_photos') ): ?>
        <ul class="slides">
          <?php while( have_rows('artist_works_photos') ): the_row(); 
		// vars
		if( have_rows('image_details') ):
		 while( have_rows('image_details') ): the_row(); 
		$image = get_sub_field('image');
		?>
          <li class="slide clone"><img src="<?php echo $image['url'];?>" alt="<?php the_title();?>" draggable="false"></li>
          <?php endwhile; endif; endwhile; ?>
        </ul>
        <?php endif;?>
        <ul class="flex-direction-nav" id="flex-nav">
          <li class="flex-nav-prev"><a class="flex-prev" href="#">
            <div><i class="fa fa-angle-left"></i></div>
            </a></li>
          <li class="flex-nav-next"><a class="flex-next" href="#">
            <div><i class="fa fa-angle-right"></i></div>
            </a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="column2">
    <div class="column_inner">
      <div class="portfolio_single_text_holder">
      	<p class="title"><?php the_title();?></p>
        <?php the_content(); ?>
        <div class="clearfix"></div>
                    <?php 
						$post_object = get_field('select_artist_profile');
						if($post_object) {
						$post = $post_object;
						setup_postdata( $post ); 
						//var_dump($post_object);
					?>
                    <?php if(ICL_LANGUAGE_CODE=='ja') {?>                    
                    <div class="artist_profile"><p><a href="<?php the_permalink(); ?>">PROFILE</a></p></div>
                    <?php } else {?>
                    <div class="artist_profile"><p><a href="<?php the_permalink(); ?>">PROFILE</a></p></div>
                    <?php }?>
                    <?php } wp_reset_postdata();?>
                    <?php $artist_website = get_field('artist_website_url'); if($artist_website) {?>
                    <div class="artist_profile"><p><a href="<?php echo $artist_website; ?>" target="_blank">ARTIST’S WEBSITE</a></p></div>
                    <?php }?>
                    <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="portfolio_single <?php echo esc_attr($portfolio_class); ?>">
    <div class="portfolio_gallery">
      <?php if( have_rows('artist_works_photos') ): 
				$artist_works = get_field('artist_works_photos');
				$order = array();
				// populate order
			foreach( $artist_works as $i => $row1 ) {
				$order[ $i ] = $row1['date']; 
			?>
			<?php }
			array_multisort( $order, SORT_DESC, $artist_works );
			// loop through repeater
			if( $artist_works ){ 
					 ?>
				   
					<?php foreach( $artist_works as $i => $row1 ): 
					//var_dump($row);
					$format_out = 'Y';
					$date = $row1['date'];
					$date_interval = $row1['date_interval'];
					?>
                    <div class="artist-all-work" id="<?php echo $date;?>">
                     <div class="date"><div class="timestamp"><?php if($date_interval) { echo $date_interval; } else { echo $date; }?></div></div>
                     
                     <?php 
					 		$image_details = $row1['image_details'];
							if($image_details) { ?>
                            <?php if(ICL_LANGUAGE_CODE=='en') {?>
							 <ul class="cbp-rfgrid">
                            <?php	
							foreach( $image_details as $i => $row ) {
							$image_src = $row['image']['url'];
							$sizes = $row['image']['sizes']['large'];	
                      ?>
            <li> 
<a class="lightbox_single_portfolio" href="<?php echo $image_src; ?>" rel="prettyPhoto[single_pretty_photo]" title="<?php echo $row['caption']; ?>">
<img src="<?php echo $image_src; ?>" srcset="<?php echo $sizes; ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?php echo $row['caption']; ?>">
</a> 
            </li>
      <?php } wp_reset_postdata(); 
	  $video_details = $row1['video_details'];
	   foreach( $video_details as $i => $row1 ) {
		$video_link = $row1['youtube_link'];
		$video_id = basename($video_link).PHP_EOL;   
	?><li>
      <a class="lightbox_single_portfolio video_in_lightbox" href="<?php echo $video_link;?>" title="" rel="prettyPhoto[single_pretty_photo]">
      <i class="fa fa-play"></i><img width="100%" src="http://img.youtube.com/vi/<?php echo $video_id;?>/0.jpg" alt="" title=""></img></a>
      </li>
       <?php }?>
        </ul>
      <?php } else { ?>
      <div class="cbp-rfgrid">
                            <?php	
							foreach( $image_details as $i => $row ) {
							$image_src = $row['image']['url'];
							$sizes = $row['image']['sizes']['large'];	
                      ?>
<a class="example-image-link" href="<?php echo $image_src; ?>" data-lightbox="example-set" data-title="<?php echo $row['caption']; ?>">
<img src="<?php echo $image_src; ?>" srcset="<?php echo $sizes; ?>" sizes="(max-width: 50em) 87vw, 680px" alt="<?php echo $row['caption']; ?>"> 
              </a> 
      <?php } wp_reset_postdata(); 
	  $video_details = $row1['video_details'];
	   foreach( $video_details as $i => $row1 ) {
		$video_link = $row1['youtube_link'];
		$video_id = basename($video_link).PHP_EOL;   
	?><a class="lightbox_single_portfolio video_in_lightbox" title="<?php echo $video_title; ?>" href="<?php echo $video_link;?>" rel="prettyPhoto[single_pretty_photo]"><i class="fa fa-play"></i><img width="100%" src="http://img.youtube.com/vi/<?php echo $video_id;?>/0.jpg"></img></a>
       <?php }?>
      </div>
      
      <?php } ?>
      <?php } ?> 
      </div>
      <?php endforeach; ?>
      <div class="clearfix"></div>
      
      <?php } endif; wp_reset_query();?>
    </div>
        
    <?php
       
	}
	?>
  </div>
  <?php } ?>
</div>
