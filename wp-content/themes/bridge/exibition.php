<?php 
/*
Template Name: Exihibition
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
<?php get_header(); ?>
<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
<?php } ?>
<?php get_template_part( 'title' ); ?>
<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>

<div class="q_slider">
  <div class="q_slider_inner"> <?php echo do_shortcode($revslider); ?> </div>
</div>
<?php
		}
		?>
<?php if ( is_page( 'current' ) || is_page( 'upcomming' )) { 
?>
<div class="clearfix"></div>
<style>
#nav-menu-item-15517 a
{
    color: #95d0ef !important;	
}
li.slide.clone.flex-active-slide div#portfolio-content{

}
#slider-two ul.flex-direction-nav
{
	display:none;
}
.flexslider .flex-caption {

  transform: translateX(-50%) translateY(-50);
}
.flex-direction-nav
{
	visibility:hidden;
}
#flex-nav
{
	visibility:visible;
}
.exibition_inner h3.title
{
	text-align:left;
	font-size:14px;
	padding:5px 0px;
	height:auto;	
	text-transform: uppercase;
	color:#000 !important;
}
.exibition_inner h3.title span
{
	color:#000;
}
.flex-direction-nav a, .flexslider .flex-prev, .portfolio_slider .flex-prev, .flexslider .flex-next, .portfolio_slider .flex-next, body div.pp_default a.pp_next:after, body div.pp_default a.pp_previous:after, body a.pp_next:after, body a.pp_previous:after, .wpb_gallery .wpb_wrapper .wpb_flexslider .flex-direction-nav a
{
	background:transparent !important;
}
.flex-direction-nav a:hover, .flexslider .flex-prev:hover, .portfolio_slider .flex-prev:hover, .flexslider .flex-next:hover, .portfolio_slider .flex-next:hover, body div.pp_default a.pp_next:hover:after, body div.pp_default a.pp_previous:hover:after, body a.pp_next:hover:after, body a.pp_previous:hover:after, .flexslider:hover .flex-direction-nav a.flex-prev:hover, .flexslider:hover .flex-direction-nav a.flex-next:hover, .portfolio_slider:hover .flex-direction-nav a.flex-prev:hover, .portfolio_slider:hover .flex-direction-nav a.flex-next:hover, .wpb_gallery .wpb_flexslider .flex-direction-nav a:hover
{
	background:transparent !important;
	border:transparent !important;
}
.exibition_inner
{
	padding:0 45px;
}
.exibition_inner #exhibition-post .column2
{
	width:32.33%;
}
</style>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function ($) {
$('#slider-one').flexslider({
    animation: "fade",
	controlsContainer: $("#flex-nav"),
    customDirectionNav: $("#flex-nav a")
	/*,
	sync: "#slider-two"*/
  });
    
  /*$('#slider-two').flexslider({
    animation: "slide",
    controlNav: false,
	slideshow: false,
	pauseOnHover: true,
	directionNav: true
  });*/
  
  
 
  
  
});
</script>
<?php $category = get_the_category(); 
$PageName = $wp_query->queried_object->post_name;
if(ICL_LANGUAGE_CODE=='ja'){ 
	if ($PageName == 'current'){ $PageName = 'current-ja';
	} elseif($PageName == 'upcomming') {$PageName = 'upcomming-ja';
	} else {$PageName = 'past-ja'; }
}
$args = array(
    'post_type' => 'exihibition',
    'tax_query' => array(
        array(
            'taxonomy' => 'exihibition_category',
            'field' => 'slug',
            'terms' => $PageName,
			'order' => 'ASC'
        )
    )
);
$loop = new WP_Query( $args );
//var_dump($loop);
if(ICL_LANGUAGE_CODE=='ja'){ 
if($PageName == 'current-ja') { $PageName = 'current';}elseif ($PageName == 'upcomming-ja') { $PageName = 'upcomming'; } else {$PageName = 'past';} 
}
?>
<div class="full_width">
  <div class="full_width_inner">
    <div class="exibition_inner default_template_holder clearfix">
      <h3 class="title"><span><?php echo $PageName;?> EXHIBITION</span></h3>
    </div>
  </div>
</div>
<div class="exibition_inner default_template_holder clearfix">
  <div class="portfolio_single">
    <div class="clearfix"></div>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); 
  				$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	?>
    <div class="two_columns_66_33 clearfix portfolio_container" id="exhibition-post">
      <div class="column1">
        <div class="column_inner"> 
          <!-- Place somewhere in the <body> of your page -->
          
          <?php if( have_rows('slider_images') ){ ?>
          <div class="flexslider" id="slider-one">
            <ul class="slides">
              <?php 
					// loop through the rows of data
					while ( have_rows('slider_images') ) : the_row();
						// display a sub field value
						$SliderImg = get_sub_field('image');
						$SliderImgLink = get_sub_field('link');
					?>
              <li class="slide clone">
              <?php if($SliderImgLink) { ?>
              <a href="<?php echo $SliderImgLink;?>">
              <img src="<?php echo $SliderImg['url'];?>" alt="<?php the_title();?>" draggable="false">
              </a>
              <?php } else { ?><img src="<?php echo $SliderImg['url'];?>" alt="<?php the_title();?>" draggable="false"><?php } ?>
              </li>
              <?php endwhile;?>
            </ul>
            <ul class="flex-direction-nav" id="flex-nav">
              <li class="flex-nav-prev"><a class="flex-prev" href="#">
                <div><i class="fa fa-angle-left"></i></div>
                </a></li>
              <li class="flex-nav-next"><a class="flex-next" href="#">
                <div><i class="fa fa-angle-right"></i></div>
                </a></li>
            </ul>
          </div>
          <?php } else { ?>
          <div class="feat-image"> <img src="<?php echo $feat_image;?>" alt="<?php the_title();?>"></div>
          <?php }?>
        </div>
      </div>
      
      <!--content 1 ends here-->
      <div class="column2">
        <div class="column_inner">
          <div id="portfolio-content" class="portfolio-details">
            <div class="portfolio_detail <?php echo $portfolio_text_follow; ?> clearfix">
              <h6 class="exibition-title"><?php the_title();?></h6>
              <?php
							$portfolios = get_post_meta(get_the_ID(), "qode_portfolios", true);
							if ($portfolios){
								usort($portfolios, "comparePortfolioOptions");
								foreach($portfolios as $portfolio){
									?>
              <div class="info portfolio_custom_field">
                <?php if($portfolio['optionLabel'] != ""): ?>
                <h6><?php echo stripslashes($portfolio['optionLabel']); ?></h6>
                <?php endif; ?>
                <p>
                  <?php if($portfolio['optionUrl'] != ""): ?>
                  <a href="<?php echo $portfolio['optionUrl']; ?>" target="_blank"> <?php echo do_shortcode(stripslashes($portfolio['optionValue'])); ?> </a>
                  <?php else:?>
                  <?php echo do_shortcode(stripslashes($portfolio['optionValue'])); ?>
                  <?php endif; ?>
                </p>
              </div>
              <?php
								}
							}
							?>
              <?php if(get_post_meta(get_the_ID(), "qode_portfolio_date", true)) : ?>
              <div class="info portfolio_custom_date">
                <h6>
                  <?php _e('Date','qode'); ?>
                </h6>
                <p><?php echo get_post_meta(get_the_ID(), "qode_portfolio_date", true); ?></p>
              </div>
              <?php endif; ?>
              <?php
							$terms = wp_get_post_terms(get_the_ID(),'portfolio_category');
							$counter = 0;
							$all = count($terms);
							if($all > 0){
								?>
              <div class="info portfolio_categories">
                <h6>
                  <?php _e('Category ','qode'); ?>
                </h6>
                <span class="category">
                <?php

													foreach($terms as $term) {
														$counter++;
														if($counter < $all){ $after = ', ';}
														else{ $after = ''; }
														echo $term->name.$after;
													}
													?>
                </span> </div>
              <?php } ?>
              <?php
							$portfolio_tags = wp_get_post_terms(get_the_ID(),'portfolio_tag');

							if(is_array($portfolio_tags) && count($portfolio_tags)) {
								foreach ($portfolio_tags as $portfolio_tag) {
									$portfolio_tags_array[] = $portfolio_tag->name;
								}

								?>
              <div class="info portfolio_tags">
                <h6>
                  <?php _e('Tags', 'qode') ?>
                </h6>
                <span class="category"> <?php echo implode(', ', $portfolio_tags_array) ?> </span> </div>
              <?php } ?>
              <?php if($disable_portfolio_single_title_label) { ?>
              <h6><?php echo _e('About This Project','qode'); ?></h6>
              <?php } ?>
              <div class="info portfolio_content">
                <?php the_content(); ?>
              </div>
              <div class="clearfix"></div>
              <?php 
						$post_object = get_field('select_artist_profile');
						if($post_object) {
						$post = $post_object;
						setup_postdata( $post ); 
						//var_dump($post_object);
					?>
               <?php if(ICL_LANGUAGE_CODE=='ja') {?>                    
                    <div class="artist_profile"><p><a href="<?php the_permalink(); ?>">PROFILE</a></p></div>
                    <?php } else {?>
                    <div class="artist_profile"><p><a href="<?php the_permalink(); ?>">PROFILE</a></p></div>
                    <?php }?>
              <?php }?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php  endwhile; wp_reset_postdata();?>
    <div class="clearfix"></div>
  </div>
</div>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if(is_page( 'past' )) {?>
<style>
#nav-menu-item-15517 a
{
    color: #95d0ef !important;	
}

footer {
    position: fixed !important;
	bottom: 0 !important;
}
@media all and (min-width: 320px) and (max-width: 780px) {
	footer {
	position:relative !important;
}

}

</style>
<?php $PageName = $wp_query->queried_object->post_name; 
if(ICL_LANGUAGE_CODE=='ja'){ 
	if ($PageName == 'past-ja'){ 
		$PageName = 'past';
	}
}
?>
<div class="full_width">
  <div class="full_width_inner">
    <div class="exibition_inner default_template_holder clearfix">
      <h3 class="title"><span><?php echo $PageName;?> EXHIBITIONS</span></h3>
    </div>
  </div>
</div>
<div class="exibition_inner default_template_holder clearfix">
  <div class="past-exhibition-cont">
  <?php
  if(ICL_LANGUAGE_CODE=='ja'){ 
  	if ($PageName == 'past') { $PageName = 'past-ja'; } 
  }
  $args = array(
    'post_type' => 'exihibition',
    'tax_query' => array(
        array(
            'taxonomy' => 'exihibition_category',
            'field' => 'slug',
            'terms' => $PageName,
			'order' => 'ASC'
        )
    )
);
$loop = new WP_Query( $args ); 
$posts = $loop->get_posts(); ?>
<?php
$order = array();
foreach( $posts as $i => $row ) {
	$date = $row->post_date;
	$order[ $i ] = $date; 			
}
array_multisort( $order, SORT_DESC, $posts );
foreach( $posts as $i => $row ){
setup_postdata($row); 
$date = $row->post_date_gmt;	
$post_date = mysql2date("Y", $date);
$events_details = get_field('events_details', $row->ID);
$post_link = get_permalink($row->ID);
//var_dump($row);
?>
<div class="group" id="<?php echo $post_date;?>">
	<div class="year" id="<?php echo $post_date;?>"><?php echo $post_date;?></div>
    <div class="acf-data">
		<ul class="events">
            <?php
			foreach( $events_details as $events_data ) { 
				$event_start_date = $events_data['event_start_date'];
				$event_end_date = $events_data['event_end_date'];
				if(ICL_LANGUAGE_CODE=='ja') {
				$esd = date("m月 d日", strtotime($event_start_date));
				$eed = date("m月 d日", strtotime($event_end_date));
				}
				else 
				{
				$esd = date("M d", strtotime($event_start_date));
				$eed = date("M d", strtotime($event_end_date));
				}
				//echo $esd;
				//var_dump($events_data);
			?>
            <li><span><?php echo $esd;?> </span> - <span> <?php echo $eed;?></span></li>
            <?php } wp_reset_postdata();?>
         </ul>
        
         </div>
         <div class="post-title">
        	<a href="<?php echo $post_link;?>"><?php echo $row->post_title;?></a>
        </div>
</div>
<?php } wp_reset_postdata();?>
<script>
jQuery(document).ready(function ($) {
	var post_date = "<?php $post_date ;?>";
	$(".group .year").filter(function(post_date) {
    if ($(this).attr("id").match(post_date)) {
		$(this).children().first();
        $(this).css('visibility', 'hidden');
		console.log(this);
    }
});
});
</script>
<?php } ?>
<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
<?php if(($sidebar == "default")||($sidebar == "")) : ?>
<?php if (have_posts()) : 
					while (have_posts()) : the_post(); ?>
<?php //the_content(); ?>
<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
<?php
					if($enable_page_comments){
					?>
<div class="container">
  <div class="container_inner">
    <?php
						comments_template('', true); 
					?>
  </div>
</div>
<?php
					}
					?>
<?php endwhile; ?>
<?php endif; ?>
<?php elseif($sidebar == "1" || $sidebar == "2"): ?>
<?php if($sidebar == "1") : ?>
<div class="two_columns_66_33 clearfix grid2">
<div class="column1">
<?php elseif($sidebar == "2") : ?>
<div class="two_columns_75_25 clearfix grid2">
  <div class="column1">
    <?php endif; ?>
    <?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
    <div class="column_inner">
      <?php the_content(); ?>
      <?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
      <?php
							if($enable_page_comments){
							?>
      <div class="container">
        <div class="container_inner">
          <?php
								comments_template('', true); 
							?>
        </div>
      </div>
      <?php
							}
							?>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>
  <div class="column2">
    <?php get_sidebar();?>
  </div>
</div>
<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
<?php if($sidebar == "3") : ?>
<div class="two_columns_33_66 clearfix grid2">
  <div class="column1">
    <?php get_sidebar();?>
  </div>
  <div class="column2">
    <?php elseif($sidebar == "4") : ?>
    <div class="two_columns_25_75 clearfix grid2">
      <div class="column1">
        <?php get_sidebar();?>
      </div>
      <div class="column2">
        <?php endif; ?>
        <?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
        <div class="column_inner">
          <?php the_content(); ?>
          <?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
          <?php
							if($enable_page_comments){
							?>
          <div class="container">
            <div class="container_inner">
              <?php
								comments_template('', true); 
							?>
            </div>
          </div>
          <?php
							}
							?>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>
