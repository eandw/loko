<?php 
/*
Template Name: Top Page
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
<?php get_header(); ?>
<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
<?php } ?>
<?php get_template_part( 'title' ); ?>
<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>

<div class="q_slider">
  <div class="q_slider_inner"> <?php echo do_shortcode($revslider); ?> </div>
</div>
<?php
		}
		?>
<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
<div class="full_width_inner slider-top" <?php qode_inline_style($content_style_spacing); ?>>
<?php if(($sidebar == "default")||($sidebar == "")) : ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function ($) {
$('#top-slider').flexslider({
    animation: "fade",
    controlNav: false,
	pauseOnHover: false,
	controlsContainer: $("#flex-nav"),
    customDirectionNav: $("#flex-nav a")
  });
    
});
</script>
<div class="flexslider top-page" id="top-slider">
<?php if( have_rows('slider_images') ): ?>
  <ul class="slides">
    <?php while( have_rows('slider_images') ): the_row(); 
		// vars
		$image = get_sub_field('images');
		$link = get_sub_field('link');
		?>
    <li class="slide clone" aria-hidden="true">
    <?php if( $link ): ?>
				<a href="<?php echo $link; ?>">
			<?php endif; ?>
				<div class="image">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
			<?php if( $link ): ?>
				</a>
			<?php endif; ?></li>
    <?php endwhile;?>
  </ul>
  <?php endif; ?>
  <ul class="flex-direction-nav" id="flex-nav">
    <li class="flex-nav-prev"><a class="flex-prev" href="#">
      <div><i class="fa fa-angle-left"></i></div>
      </a></li>
    <li class="flex-nav-next"><a class="flex-next" href="#">
      <div><i class="fa fa-angle-right"></i></div>
      </a></li>
  </ul>
</div>
<?php //the_content(); ?>
<?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
<div class="clearfix"></div>
<div class="container">
  <div class="container_inner news-section">
    <div class="news-articles">
      <?php 
			if(ICL_LANGUAGE_CODE == 'ja'){
			$cat_slug = 'news';
			} else {
			$cat_slug = 'news-en';
			}
		global $post;
		$args = array( 'numberposts' => 10, 'category_name' => $cat_slug );
		$posts = get_posts( $args );
		?>
      <ul>
        <?php foreach( $posts as $post ): setup_postdata($post); 
		
		?>
        <li><span>
          <?php the_time('Y.m.d');?>
          </span> <!--<a href="<?php /*?><?php echo home_url();?>/news/<?php */?>#">-->
          <?php the_title();?>
          <!--</a>--></li>
        <?php endforeach;?>
      </ul>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<?php endwhile; ?>
<?php endif; ?>
<?php elseif($sidebar == "1" || $sidebar == "2"): ?>
<?php if($sidebar == "1") : ?>
<div class="two_columns_66_33 clearfix grid2">
<div class="column1">
<?php elseif($sidebar == "2") : ?>
<div class="two_columns_75_25 clearfix grid2">
  <div class="column1">
    <?php endif; ?>
    <?php if (have_posts()) : 
						while (have_posts()) : the_post(); ?>
    <div class="column_inner">
      <?php the_content(); ?>
      <?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
      <?php
							if($enable_page_comments){
							?>
      <div class="container">
        <div class="container_inner">
          <?php
								comments_template('', true); 
							?>
        </div>
      </div>
      <?php
							}
							?>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
  </div>
  <div class="column2">
    <?php get_sidebar();?>
  </div>
</div>
<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
<?php if($sidebar == "3") : ?>
<div class="two_columns_33_66 clearfix grid2">
  <div class="column1">
    <?php get_sidebar();?>
  </div>
  <div class="column2">
    <?php elseif($sidebar == "4") : ?>
    <div class="two_columns_25_75 clearfix grid2">
      <div class="column1">
        <?php get_sidebar();?>
      </div>
      <div class="column2">
        <?php endif; ?>
        <?php if (have_posts()) : 
							while (have_posts()) : the_post(); ?>
        <div class="column_inner">
          <?php the_content(); ?>
          <?php 
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
          <?php
							if($enable_page_comments){
							?>
          <div class="container">
            <div class="container_inner">
              <?php
								comments_template('', true); 
							?>
            </div>
          </div>
          <?php
							}
							?>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>
