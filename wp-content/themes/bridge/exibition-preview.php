<?php get_header(); ?>
<style>
#nav-menu-item-15517 a
{
    color: #95d0ef !important;	
}
li.slide.clone.flex-active-slide div#portfolio-content{

}
#slider-two ul.flex-direction-nav
{
	display:none;
}
.flexslider .flex-caption {

  transform: translateX(-50%) translateY(-50);
}
.flex-direction-nav
{
	visibility:hidden;
}
#flex-nav
{
	visibility:visible;
}
.exibition_inner h3.title
{
	text-align:left;
	font-size:14px;
	padding:5px 0px;
	height:auto;	
	text-transform: uppercase;
	color:#000 !important;
}
.exibition_inner h3.title span
{
	color:#000;
}
.flex-direction-nav a, .flexslider .flex-prev, .portfolio_slider .flex-prev, .flexslider .flex-next, .portfolio_slider .flex-next, body div.pp_default a.pp_next:after, body div.pp_default a.pp_previous:after, body a.pp_next:after, body a.pp_previous:after, .wpb_gallery .wpb_wrapper .wpb_flexslider .flex-direction-nav a
{
	background:transparent !important;
}
.flex-direction-nav a:hover, .flexslider .flex-prev:hover, .portfolio_slider .flex-prev:hover, .flexslider .flex-next:hover, .portfolio_slider .flex-next:hover, body div.pp_default a.pp_next:hover:after, body div.pp_default a.pp_previous:hover:after, body a.pp_next:hover:after, body a.pp_previous:hover:after, .flexslider:hover .flex-direction-nav a.flex-prev:hover, .flexslider:hover .flex-direction-nav a.flex-next:hover, .portfolio_slider:hover .flex-direction-nav a.flex-prev:hover, .portfolio_slider:hover .flex-direction-nav a.flex-next:hover, .wpb_gallery .wpb_flexslider .flex-direction-nav a:hover
{
	background:transparent !important;
	border:transparent !important;
}
.exibition_inner
{
	padding:0 45px;
}
.exibition_inner #exhibition-post .column2
{
	width:32.33%;
}
</style>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function ($) {
$('#slider-one').flexslider({
    animation: "fade",
	controlsContainer: $("#flex-nav"),
    customDirectionNav: $("#flex-nav a")
	/*,
	sync: "#slider-two"*/
  });
    
  /*$('#slider-two').flexslider({
    animation: "slide",
    controlNav: false,
	slideshow: false,
	pauseOnHover: true,
	directionNav: true
  });*/
  
  
 
  
  
});
</script>
<?php $PageName = $wp_query->queried_object->post_name;
?>
<?php if($PageName == 'past-exihibition') {?>
<style>
#nav-menu-item-16196 a, #nav-menu-item-16197 a
{
    color: #95d0ef !important;	
}
#nav-menu-item-16196 .second .inner ul li a, #nav-menu-item-16197 .second .inner ul li a{
    color: #C9C9C9 !important;
}
#nav-menu-item-16196 .second .inner ul li a:hover, #nav-menu-item-16197 .second .inner ul li a:hover{
    color: #95d0ef !important;
}
</style>
<?php
}
if(ICL_LANGUAGE_CODE == 'ja' || ICL_LANGUAGE_CODE == 'en'){ 
if($PageName == 'current-ja') { $PageName = 'current';}elseif ($PageName == 'upcomming-ja') { $PageName = 'upcomming'; } elseif($PageName == 'past-exihibition') {$PageName = 'past';} 
}
?>
<div class="full_width">
  <div class="full_width_inner">
    <div class="exibition_inner default_template_holder clearfix">
      <h3 class="title"><span><?php echo $PageName;?> EXHIBITION</span></h3>
    </div>
  </div>
</div>
<div class="content content_top_margin_none">
<div class="full_width">
  <div class="full_width_inner">
    <div class="exibition_inner default_template_holder clearfix">
  <div class="portfolio_single">
  
  <div class="clearfix"></div>
  <?php while (have_posts()) : the_post(); 
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	?>
    <div class="two_columns_66_33 clearfix portfolio_container" id="exhibition-post">
      <div class="column1">
        <div class="column_inner"> 
          <!-- Place somewhere in the <body> of your page -->
          <?php if( have_rows('slider_images') ){ ?>
          <div class="flexslider" id="slider-one">
            <ul class="slides">
             <?php 
					// loop through the rows of data
					while ( have_rows('slider_images') ) : the_row();
						// display a sub field value
						$SliderImg = get_sub_field('image');
						$SliderImgLink = get_sub_field('link');
					?>
              <li class="slide clone">
              <?php if($SliderImgLink) { ?>
              <a href="<?php echo $SliderImgLink;?>">
              <img src="<?php echo $SliderImg['url'];?>" alt="<?php the_title();?>" draggable="false">
              </a>
              <?php } else { ?><img src="<?php echo $SliderImg['url'];?>" alt="<?php the_title();?>" draggable="false"><?php } ?>
              </li>
              <?php endwhile;?>
            </ul>
            <ul class="flex-direction-nav" id="flex-nav">
              <li class="flex-nav-prev"><a class="flex-prev" href="#">
                <div><i class="fa fa-angle-left"></i></div>
                </a></li>
              <li class="flex-nav-next"><a class="flex-next" href="#">
                <div><i class="fa fa-angle-right"></i></div>
                </a></li>
            </ul>
            
          </div>
          <?php } else { ?><div class="feat-image"> <img src="<?php echo $feat_image;?>" alt="<?php the_title();?>"></div><?php }?>
        </div>
      </div>
      <!--content 1 ends here-->
      <div class="column2">
        <div class="column_inner">
                <div id="portfolio-content" class="portfolio-details">
                  <div class="portfolio_detail <?php echo $portfolio_text_follow; ?> clearfix">
                  <h6 class="exibition-title"><?php the_title();?></h6>
                    <?php
							$portfolios = get_post_meta(get_the_ID(), "qode_portfolios", true);
							if ($portfolios){
								usort($portfolios, "comparePortfolioOptions");
								foreach($portfolios as $portfolio){
									?>
                    <div class="info portfolio_custom_field">
                      <?php if($portfolio['optionLabel'] != ""): ?>
                      <h6><?php echo stripslashes($portfolio['optionLabel']); ?></h6>
                      <?php endif; ?>
                      <p>
                        <?php if($portfolio['optionUrl'] != ""): ?>
                        <a href="<?php echo $portfolio['optionUrl']; ?>" target="_blank"> <?php echo do_shortcode(stripslashes($portfolio['optionValue'])); ?> </a>
                        <?php else:?>
                        <?php echo do_shortcode(stripslashes($portfolio['optionValue'])); ?>
                        <?php endif; ?>
                      </p>
                    </div>
                    <?php
								}
							}
							?>
                    <?php if(get_post_meta(get_the_ID(), "qode_portfolio_date", true)) : ?>
                    <div class="info portfolio_custom_date">
                      <h6>
                        <?php _e('Date','qode'); ?>
                      </h6>
                      <p><?php echo get_post_meta(get_the_ID(), "qode_portfolio_date", true); ?></p>
                    </div>
                    <?php endif; ?>
                    <?php
							$terms = wp_get_post_terms(get_the_ID(),'portfolio_category');
							$counter = 0;
							$all = count($terms);
							if($all > 0){
								?>
                    <div class="info portfolio_categories">
                      <h6>
                        <?php _e('Category ','qode'); ?>
                      </h6>
                      <span class="category">
                      <?php

													foreach($terms as $term) {
														$counter++;
														if($counter < $all){ $after = ', ';}
														else{ $after = ''; }
														echo $term->name.$after;
													}
													?>
                      </span> </div>
                    <?php } ?>
                    <?php
							$portfolio_tags = wp_get_post_terms(get_the_ID(),'portfolio_tag');

							if(is_array($portfolio_tags) && count($portfolio_tags)) {
								foreach ($portfolio_tags as $portfolio_tag) {
									$portfolio_tags_array[] = $portfolio_tag->name;
								}

								?>
                    <div class="info portfolio_tags">
                      <h6>
                        <?php _e('Tags', 'qode') ?>
                      </h6>
                      <span class="category"> <?php echo implode(', ', $portfolio_tags_array) ?> </span> </div>
                    <?php } ?>
                    <?php if($disable_portfolio_single_title_label) { ?>
                    <h6><?php echo _e('About This Project','qode'); ?></h6>
                    <?php } ?>
                    <div class="info portfolio_content">
                      <?php the_content(); ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php 
						$post_object = get_field('select_artist_profile');
						if($post_object) {
						$post = $post_object;
						setup_postdata( $post ); 
						//var_dump($post_object);
					?>                    
                    <div class="artist_profile"><h6>作家情報はこちら:</h6><p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p></div>
                    <?php }?>
                    <div class="clearfix"></div>
                  </div>
                </div>
             
          </div>
        </div>
      </div>
      <?php  endwhile; wp_reset_postdata();?>
       <div class="clearfix"></div>
    </div>
   
    
  </div>
  </div>
</div>
<?php get_footer(); ?>
