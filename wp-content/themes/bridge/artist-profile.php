<style>
#nav-menu-item-15530 a, #nav-menu-item-16153 a
{
    color: #95d0ef !important;	
}
</style>
<?php get_header(); ?>
<div class="content content_top_margin_none">
<div class="full_width">
  <div class="full_width_inner">
    <div class="exibition_inner default_template_holder clearfix">
      <div class="two_columns_66_33 clearfix portfolio_container" id="artist-profile">
        <div class="column1">
          <div class="column_inner">
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <h5><?php the_title(); ?> PROFILE</h5>
            <div class="post_text">
              <div class="post_text_inner">
                <?php the_content(); ?>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="column2">
          <div class="column_inner">
            <div class="artist-photo">
              <?php if(get_post_meta(get_the_ID(), "qode_hide-featured-image", true) != "yes") {
					if ( has_post_thumbnail() ) { ?>
              <div class="post_image">
                <img src="<?php echo the_post_thumbnail_url();?>">
              </div>
              <?php } } ?>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
