<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Applications/MAMP/htdocs/loko/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'loko');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xIvPa-ahQE+f{78-<{hj~-+sQ/d:r`X{e 2Sq,FbT[bTr{}mD|zq~xsU58_)%mY2');
define('SECURE_AUTH_KEY',  '}rcNxoDD$%ICSELxj<28EXKDvnmz,g0z<K@>nln}S#|cpo+g[TY:1K(~Y%fnbK1:');
define('LOGGED_IN_KEY',    'Ci5hiEefQS?wOn|P>c jB@1CunBWHs`<rC60ZmqG$-n!`dcr|3T4s>N%]-Ar9hqj');
define('NONCE_KEY',        '!e^Z:]]j/4-!Jt|^<TK>.q8<*JJNZupFb=Gu^:Nd$L_dPDuUXO%IR_t-;_tu}LB:');
define('AUTH_SALT',        'lF|{|7I?)HwqiRx@Fd%NQ 71-`o4B;_;oA02?p$;2>-&wM.2-Y)sJDJA(fz>o?0D');
define('SECURE_AUTH_SALT', '/IIdwCrcArT^2uDjWm;szUX4$%oWK8Pr|44.Av9L+0NK)<n]*w|):49TIUFK:}5,');
define('LOGGED_IN_SALT',   '`r+gU-R+aBiinw;gG1aGp8VnZL-^pDyNu, NZ[hkJcUmK6!++{yhUMzIOvdy_?,S');
define('NONCE_SALT',       'IgP_^o6*j,!;@j$+( WmO2i5q-Y$!r7Ek7zZG_5-6VCHz0**^{IDFrByrN+w/;kU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
